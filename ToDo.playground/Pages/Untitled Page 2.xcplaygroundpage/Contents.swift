//: Playground - noun: a place where people can play

import UIKit

enum Date {
    
    case today
    case tomorow
    case thisWeek
    case thisMonth
    
}

enum Priority {
    
    case low
    case middle
    case high
    case urgent
}


struct Item {
    
    let itemText: String
    var itemOrder: Int
    let itemPriority: Priority
    var toDoDate: Date
    var complite: Bool = false
    
}

extension Item {
    
    init(itemText: String) {
        self.itemText = itemText
    }
    
    init(itemText: String, itemPriority: Priority) {
        self.itemText = itemText
        self.itemPriority = itemPriority
    }
    
    init(itemText: String, itemPriority: Priority, itemOrder: Int) {
        self.itemText = itemText
        self.itemPriority = itemPriority
        self.itemOrder = itemOrder
    }
    
    init(itemText: String, toDoDate: Date) {
        self.itemText = itemText
        self.toDoDate = toDoDate
    }
    
    
    
}

//var nestoDrugo = Item(itemText: "nesto nesto")
//var nesto = Item(itemText: "nesto uradi", itemOrder: 1, itemPriority: .middle, toDoDate: .tomorow, complite: false)


//nesto.complite
//nesto.itemPriority
//nesto.itemOrder

enum Location {
    
    case work
    case home
    case coffeeShop
    case school
    
}

struct Activity {
    
    var ime: String
    var itemList: String
    var location: Location?
    
}







